# writelink: the missing link command
`ln` covers most common usecases, but there are some edge cases that make it annoying to use in some scripts.

`writelink` is not intended for interactive use, but instead as a low-level interface for shell scripts.

## differences from ln

* does not derefrence symlinks, compose with realpath if that is desired.
* does not check if the target exists

## advantages over ln

* has a "replace existing symlink, but don't delete regular file" mode, useful for easily installing dotfiles without accidentlly deleting an existing config
* allows easily creating symlinks to other symlinks, or to nonexistant files
