use clap::Parser;

#[derive(Parser)]
#[command(version, about)]
pub struct Cli {
	/// file that will be symlinked to
	pub target: String,
	/// name of the symlink to create
	pub link_name: String,
	/// if the target already exists AND is a symbolic link, replace it
	#[arg(long, short)]
	pub modify: bool,
}
