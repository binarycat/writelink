use std::os::unix::fs::symlink;
use std::fs::{symlink_metadata, remove_file};
use clap::Parser;

mod cli;

fn main() -> Result<(), Box<dyn std::error::Error>> {
	let cli = cli::Cli::parse();
	if cli.modify {
		if let Ok(md) = symlink_metadata(&cli.link_name) {
			if md.is_symlink() {
				remove_file(&cli.link_name)?;
			}
		}
	}
    symlink(cli.target, cli.link_name)?;
	Ok(())
}
